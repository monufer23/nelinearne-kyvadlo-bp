clear;
clc;


%%%% saving images
% flag: 0 animated gif NOT saved or 1 saved /  file name
   f_gif = 0;
   ag_name = 'pendulum.gif';
% Delay in seconds before displaying the next image  
   delay = 0;  
% Frame counter start
   nt = 1; 
 
   

% q - uhol, g/l = w^2
% Rovnica: q'' + g/l*sin(q) = 0
%          q'' + w^2*sin(q) = 0

% x(1) = x(2)
% x(2) = -w*sin(x(1))

% g - gravitacna sila, 
% l - dlzka kyvadla

N = 250;
t = linspace(0,2,N);

%t = 0:0.001:25; % time spec
initX   = [pi/12 0]; % initial conditions

[t,x] = ode45(@myFun, t, initX);
xx = x(:,1);
vv = x(:,2);


for c = 1:3*N/4   
    figure(1)                   %creates figure window

    fs      = 10;               %font size
  %  set(gcf,'Name','Volny pad');
    set(gcf,'units','normalized','position',[0.1 0.1 0.5 0.9]); %x0, y0%, width, heigth

    
    xP      = t; 
    yP      = xx;                       %vykreslujem drahu
    pos1    = [0.25 0.55 0.7 0.35];           %x0 y0 width height
   
    subplot('Position',pos1);   
    plot(xP,yP,'b','lineWidth',2);
    set(gca,'fontsize',fs);            %dotazujem sa na moje aktualne osi
 %  xlabel('t - time');
    ylabel('V�chylka');
    grid on
    hold on
 
 
   yP   = x(:,2);                       % vykreslujem rychlost
   pos2 = [0.25 0.15 0.7 0.35];
   subplot('Position',pos2);
   plot(xP,yP,'b','lineWidth',2);
   set(gca,'fontsize',fs);            %dotazujem sa na moje aktualne osi
   %ylabel('R�chlos�');
   grid on
   hold on
   %ylabel('x(t) [ m ]');
   
   xP = t(1:c); 
   yP = x(1:c,1);
   pos1 = [0.25 0.55 0.7 0.35];
   subplot('Position',pos1);
   plot(xP,yP,'r','lineWidth',3);
   
  % xlabel('t - time');
  % ylabel('Amplitude');
   grid on
   hold on
  
   xP = t(1:c); 
 % xP = x(1:c,1);
   yP = x(1:c,2);
   pos2 = [0.25 0.15 0.7 0.35];
   subplot('Position',pos2);
   plot(xP,yP,'r','lineWidth',3);
   grid on
   hold on
   %ylabel('x(t) [ m ]');
   xlabel('t - �as');
   ylabel('R�chlos�');

   xP = sin(x(c,1));
   yP = 1-cos(x(c,1)); 
   pos4 = [0.05 0.15 0.1 0.75];  
   subplot('Position',pos4);
   plot([sin(x(c,1)) 0],[1-cos(x(c,1)) 1], 'b','lineWidth', 3);
   hold on
   hPlot = plot(xP,yP,'o');
   set(gca,'xLim',[-initX(1)-0.2 initX(1)+0.2]);
   set(gca,'yLim',[-0.2 1.3]);

   set(hPlot,'MarkerFacecolor','r','MarkerSize',25)
   hold on
   hold off
   %axis off
   pause(0.001)
 
    if f_gif > 0 
       frame = getframe(1);
       im = frame2im(frame);
       [imind,cm] = rgb2ind(im,256);
     %  On the first loop, create the file. In subsequent loops, append.
       if nt == 1
          imwrite(imind,cm,ag_name,'gif','DelayTime',delay,'loopcount',inf);
       else
         imwrite(imind,cm,ag_name,'gif','DelayTime',delay,'writemode','append');
       end
       nt = nt+1;
    end
end


function dxdt = myFun(t,x)
    
    g = 9.81; 
    l = 1;
    w = g/l;
    
    dxdt_1 = x(2);
    dxdt_2 = -w^2*sin(x(1));
    
    dxdt = [dxdt_1; dxdt_2];
end