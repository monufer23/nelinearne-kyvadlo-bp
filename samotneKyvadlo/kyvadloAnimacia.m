clear;
clc;


%%%% saving images
% flag: 0 animated gif NOT saved or 1 saved /  file name
   f_gif = 0;
   ag_name = 'pendulum.gif';
% Delay in seconds before displaying the next image  
   delay = 0;  
% Frame counter start
   nt = 1; 

N = 250;
t = linspace(0,2,N);

initX1 = [pi/2 0]; % initial conditions

[t,x1] = ode45(@systemDR, t, initX1);

for c = 1:3*N/7   
    figure(1)                   %creates figure window

    fs      = 10;               %font size
    set(gcf,'units','normalized','position',[0.1 0.1 0.3 0.8]); %x0, y0%, width, heigth

   xP1 = sin(x1(c,1));
   yP2 = 1-cos(x1(c,1)); 
   pos4 = [0.05 0.15 0.9 0.75];    
   subplot('Position',pos4);
   plot([sin(x1(c,1)) 0],[1-cos(x1(c,1)) 1], 'b','lineWidth', 3);
   hold on;
   hPlot = plot(xP1,yP2,'o');
   set(gca,'xLim',[-initX1(1)-0.2 initX1(1)+0.2]);
   set(gca,'yLim',[-0.2 1.3]);
   hold off;
   set(hPlot,'MarkerFacecolor','r','MarkerSize',25)
   
  
   hold on
   hold off
   %axis off
   pause(0.001)
 
    if f_gif > 0 
       frame = getframe(1);
       im = frame2im(frame);
       [imind,cm] = rgb2ind(im,256);
     %  On the first loop, create the file. In subsequent loops, append.
       if nt == 1
          imwrite(imind,cm,ag_name,'gif','DelayTime',delay,'loopcount',inf);
       else
         imwrite(imind,cm,ag_name,'gif','DelayTime',delay,'writemode','append');
       end
       nt = nt+1;
    end
end

function dxdt = systemDR(t,x)
    g = 9.81;
    l = 1;
    w = g/l;
    b = 0.8; % dumping constant
    
    dxdt_1 = x(2);
    dxdt_2 = - w^2*sin(x(1));
    
    dxdt = [dxdt_1; dxdt_2];
end